#!/bin/bash
name="BirdFlocking.plain"

echo "Performing test '"$name"'"

rm -f BirdFlocking.mp4
../seq2vid ../examples/BirdFlocking.seq >/dev/null 2>&1

NewChecksum=`md5sum BirdFlocking.mp4 | awk '{print $1}'`
OldChecksum=$(getChecksum $name)

if [ "$NewChecksum" == "$OldChecksum" ]
then
	successCount=$(( successCount + 1 ))
	echo PASSED
else
	failCount=$(( failCount + 1 ))
	echo FAILED
fi
rm BirdFlocking.mp4
