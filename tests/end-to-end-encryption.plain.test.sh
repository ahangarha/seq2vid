#!/bin/bash
name="end-to-end-encryption.plain"

echo "Performing test '"$name"'"

rm -f end-to-end-encryption.mp4
../seq2vid ../examples/end-to-end-encryption.seq >/dev/null 2>&1

NewChecksum=`md5sum end-to-end-encryption.mp4 | awk '{print $1}'`
OldChecksum=$(getChecksum $name)

if [ "$NewChecksum" == "$OldChecksum" ]
then
	successCount=$(( successCount + 1 ))
	echo PASSED
else
	failCount=$(( failCount + 1 ))
	echo FAILED
fi
rm end-to-end-encryption.mp4
