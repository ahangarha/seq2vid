#!/bin/bash
name="best-memories.summer"

echo "Performing test '"$name"'"

rm -f best-memories.mp4
../seq2vid ../examples/best-memories.seq ../examples/summer.tmpl >/dev/null 2>&1

NewChecksum=`md5sum best-memories.mp4 | awk '{print $1}'`
OldChecksum=$(getChecksum $name)

if [ "$NewChecksum" == "$OldChecksum" ]
then
	successCount=$(( successCount + 1 ))
	echo PASSED
else
	failCount=$(( failCount + 1 ))
	echo FAILED
fi
rm best-memories.mp4
