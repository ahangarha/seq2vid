## ========================================================
## global settings for the entire video output  
if [ -z "$videoX" ]
then
videoX=640	#640	#854	#1280	#2560
videoY=480	#360	#480	#720	#1440
fi
codec="libx264" # video codec for the output: mjpeg (Motion-JPEG), libx264 (H.264), mpeg4 (MPEG-4), libvpx (VP8/VP9), etc.
FPS="24" 		# FPS (frames per second) rate of the final video

## Fonts:
Sans="Purisa"
Serif="Purisa"
Mono="FreeMono"
Regular=""
Bold="-bold"
Italic="-italic"
Oblique="-oblique"
## Colors:
text_color='#992222'	# (this the color of the current FZJ logo)
bg_color='#ffffbb'	# background color, like 'red', 'white', or in RGB units like '#005b82'

## ========================================================
## defaul values for each segment type

time_slide=2500		# default time for all segment (except video segments), in miliseconds
time_transition=1500	# default transition time between segments, in miliseconds
time_transition_pre=-1	# default transition time that overrides transition time for the previous segment (-1 means no overrding), in miliseconds
margin="10"		# default minimum distance between text/image and left/right borders, in percents (can be overrided with 'tight' options)
transition_style='morph'	# morph	# fade

## For title segments ==============
text_title=""
text_subtitle=""
text_authors=""
text_reference=""

typeface_title=$Sans$Bold
typeface_subtitle=$Sans$Regular
typeface_authors=$Sans$Regular
typeface_reference=$Serif$Italic

fontsize_title="7"	# font size, in percentage of video height
fontsize_subtitle="5"
fontsize_authors="4"
fontsize_reference="4"

y_title="30"		# the 'y' (distance from top) of the title text, in percentage of the video height
y_subtitle="60"
y_authors="58"
y_reference="70"

## For text segments ===============
text_body=""
text_subbody=""

typeface_body=$Sans$Bold
typeface_subbody=$Sans$Regular

fontsize_body="6"
fontsize_subbody="5"

y_body="45"
y_subbody="55"

## For image segments ===============
text_caption=""
imagefile=""

typeface_caption=$Sans$Regular
fontsize_caption="5"
y_caption="3"
align_image="center"	# where to place the image in the slide: center, SouthWest, NorthEast, etc.
resize_image="fit"	# how to resize the image: fit, fit-tight, shrink, shrink-tight, "<percent>%", orig (tight options neglect margins)
offsetX=0		# horizontal displacement of the image from the center of the frame
offsetY=0		# vertical displacement of the image from the center of the frame

## For video segments ===============
videofile=""

## For text block segments ===============
text_blocktitle=""
text_blocktext=""

typeface_blocktitle=$Sans$Bold
typeface_blocktext=$Sans$Regular

fontsize_blocktitle="5"
fontsize_blocktext="5"

y_blocktitle="10"
y_blocktext="20"

## ========================================================
## definition of the default background canvas for title and text segments

slide_template=slide_template"${imgsuffix}"
# For this 'plain' template, we use an empty canvas as the background of title/text segments:
$convert -size ${videoX}x${videoY} xc:${bg_color} "${slide_template}"

## ========================================================
#PRE  --> NEVER DELETE OR CHANGE THIS LINE!
# (segments described after the above line until the 'POST' line, will be added to the beginning of the user-defined sequence file)

## ========================================================
#POST  --> NEVER DELETE OR CHANGE THIS LINE!
# (segments described after the above line will be added to the end of the user-defined sequence file)
