#!/bin/bash
#seq2vid : Compose a video out of a text sequence
#Copyright (C) 2014-2017 Masoud Abkenar

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

imgsuffix=".jpg"	# The internal image format that is used to make slides.
defaultseqfile="${ExamplesPath}${PathSep}demo.seq"
defaulttemplatefile="${ExamplesPath}${PathSep}plain.tmpl"

check_dependencies

hi_message	# prints information about software and the license

echo ""
# === sequence file ===
if [ $# -eq 0 ]
then
	seqfile="$defaultseqfile"
	echo "No sequence file given; Reading from '$defaultseqfile'..."
else
	# seqfile="$1"
	seqfile=$( absolute_path "$1" )
	SequencePath=${seqfile%/*}
	if [ -e "$seqfile" ]
	then
		echo "Reading from sequence file '$seqfile'..."
	else
		echo "Sequence file '$seqfile' not found. Aborting."
		echo ""
		exit 1
	fi
fi
# === template file ===
if [ $# -eq 2 ]
then
	templatefile=$( absolute_path "$2" )
	if [ -e "$templatefile" ]
	then
		echo "Reading from template file '$templatefile'..."
	else
		echo "Template file '$templatefile' not found. Aborting."
		echo ""
		exit 1
	fi
else
	templatefile="$defaulttemplatefile"
	echo "No template file given; Using the default template in '$defaulttemplatefile'..."
fi
echo ""

# ==== Splitting the template file to temp_pre, temp_post, and temp_constants =====
total_template_lines=`wc -l "${templatefile}" | awk '{print $1}'`
pre_indicator=`grep -n "#PRE" "${templatefile}"`
post_indicator=`grep -n "#POST" "${templatefile}"`
if [ -z "${pre_indicator}" ]
then
	pre_line_no=1
else
	pre_line_no=${pre_indicator%:*}
fi
if [ -z "${post_indicator}" ]
then
	post_line_no=$total_template_lines
else
	post_line_no=${post_indicator%:*}
fi

head -$(( pre_line_no )) "${templatefile}" > template_constants.sh
tail -$(( total_template_lines - post_line_no )) "${templatefile}" > template_post.sh
tail -$(( total_template_lines - pre_line_no )) "${templatefile}" | head -$(( post_line_no - pre_line_no )) > template_pre.sh

## ========== setting output video size
## reading video dimensions from the input videos (if there are more than one, from the last video file)
touch temp.sh
grep "videofile" "$seqfile"| grep "=" >> temp.sh
. temp.sh
if [ ! -z "$videofile" ] 
then
	videofile=$(get_file "$videofile")
	videoX=$(video_width "$videofile")
	videoY=$(video_height "$videofile")
fi
rm temp.sh

## reading video dimensions from the sequence file:
touch temp.sh
grep "videoX" "$seqfile" | grep "=" >> temp.sh
grep "videoY" "$seqfile" | grep "=" >> temp.sh
. temp.sh
rm temp.sh

## laoding the settings (including possibly the video dimensions) from the template file
. template_constants.sh

## checking videoX and videoY if they are valid numerical values:
re='^[0-9]+$'
if ! [[ $videoX =~ $re ]] || ! [[ $videoY =~ $re ]] ; then
   echo "Video dimensions (videoX and/or videoY) are missing or invalid. Set/fix them in the file '$templatefile'. Aborting."
   echo ""
   exit 1
fi

## =========== initialize the main counter
frame=0
