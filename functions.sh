#!/bin/bash
#seq2vid : Compose a video out of a text sequence
#Copyright (C) 2014-2017 Masoud Abkenar

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

function hi_message {
	echo ""
	echo "seq2vid: Compose a video out of a text sequence"
	echo "Copyright (C) 2014-2017 Masoud Abkenar. Licensed under GPLv3+."
}

function check_dependencies {
	ffmpeg=`which ffmpeg`
	ffprobe=`which ffprobe`
	if ! [ -n "$ffmpeg" ]; then
		# ffmpeg doesn't exist, we try avconv:
		ffmpeg=`which avconv`
		ffprobe=`which avprobe`
		if ! [ -n "$ffmpeg" ]; then
			# neither ffmpeg nor avconv exits! We quit!
			echo "Dependencies 'avconc' or 'ffmpeg' do not exist."
			echo "In Ubuntu/Debian/Mint you must install them by "
			echo "sudo apt-get install ffmpeg"
			echo "or"
			echo "sudo apt-get install libav-tools"
			exit 1;
		fi
	fi
	mencoder=`which mencoder`
	if ! [ -n "$mencoder" ]; then
		# mencoder doesn't exist!
		echo "Dependency 'mencoder' does not exist."
		echo "In Ubuntu/Debian/Mint you must install them by "
		echo "sudo apt-get install mencoder"
		echo "Aborting."
		exit 1;
	fi
	convert=`which convert`
	if ! [ -n "$convert" ]; then
		# imagemagick doesn't exist!
		echo "Dependency 'imagemagick' does not exist."
		echo "In Ubuntu/Debian/Mint you must install them by "
		echo "sudo apt-get install imagemagick"
		echo "Aborting."
		exit 1;
	fi
}
function add_segment {

segment_options="Options are '-title', '-text', '-textblock', '-image', and '-video'."

if [ $# -eq 0 ]
then 
	echo "Please give a segment type. "$segment_options;echo "";exit;
fi
segment_type=$1

frame_str=`printf '%03d' $frame`
slidefile=slide${frame_str}${imgsuffix}

st[frame]=$segment_type
ts[frame]=$time_slide
tt[frame]=$time_transition
TransitionStyle[frame]=$transition_style
if [ "$time_transition_pre" -ne -1 ] && [ ! -z "$time_transition_pre" ]
then 
	tt[$(( frame-1 ))]=$time_transition_pre
fi

if [ $segment_type = "-title" ]
then
	# starting from the template:
	cp "${slide_template}" "${slidefile}"

	# for title
	add_text "${slidefile}" "${text_title}" "${typeface_title}" "${fontsize_title}" "${text_color}" "${y_title}"
	#for subtitle
	add_text "${slidefile}" "${text_subtitle}" "${typeface_subtitle}" "${fontsize_subtitle}" "${text_color}" "${y_subtitle}"
	#for authors
	add_text "${slidefile}" "${text_authors}" "${typeface_authors}" "${fontsize_authors}" "${text_color}" "${y_authors}"
	#for reference
	add_text "${slidefile}" "${text_reference}" "${typeface_reference}" "${fontsize_reference}" "${text_color}" "${y_reference}"

	echo $frame "- title slide added:" "${text_title}"

elif [ $segment_type = "-text" ]
then
	# starting from the template:
	cp $slide_template $slidefile

	# for text body
	add_text "${slidefile}" "${text_body}" "${typeface_body}" "${fontsize_body}" "${text_color}" "${y_body}"
	# for text subbody
	add_text "${slidefile}" "${text_subbody}" "${typeface_subbody}" "${fontsize_subbody}" "${text_color}" "${y_subbody}"

	echo $frame "- text slide added:" "${text_body}"

elif [ $segment_type = "-video" ]
then
	vf[frame]=$( get_file "${videofile}" )
	
	# if start_time empty -> start_time = 0
	if [ -z $start_time ]; then start_time=0; fi
	# if end_time empty -> end_time = video duration
	if [ -z $end_time ]; then end_time=`ffprobe -loglevel quiet -show_entries stream=duration -of default=noprint_wrappers=1:nokey=1 ${vf[frame]}`; fi
	StartTime[frame]=$start_time
	EndTime[frame]=$end_time
	video_start_frame "${vf[frame]}" ${start_time} "${slidefile}"

	echo $frame "- video slide added:" "${videofile}"

elif [ $segment_type = "-image" ]
then
	if[frame]=$( get_file "${imagefile}" )

	# starting from a plain canvas:
	$convert -size ${videoX}x${videoY} xc:${bg_color} "${slidefile}"
	# adding the image
	add_image "${slidefile}" "${if[$frame]}" "$resize_image" "$align_image" $offsetX $offsetY
	# adding image caption
	add_text "${slidefile}" "${text_caption}" "${typeface_caption}" "${fontsize_caption}" "${text_color}" "${y_caption}"

	echo $frame "- image slide added:" "${imagefile}"
elif [ $segment_type = "-textblock" ]
then
	# starting from the template:
	cp $slide_template $slidefile

	# for block title
	add_text "${slidefile}" "${text_blocktitle}" "${typeface_blocktitle}" "${fontsize_blocktitle}" "${text_color}" "${y_blocktitle}"
	# for text subbody
	add_text "${slidefile}" "${text_blocktext}" "${typeface_blocktext}" "${fontsize_blocktext}" "${text_color}" "${y_blocktext}" "west"

	echo $frame "- text block slide added:" "${text_blocktitle}"

else
	echo "Unknown segment type '"$segment_type"'. Aborting."$segment_options
	echo
	exit 1
fi

frame=$(( frame+1 ))

. template_constants.sh
}

function add_text {
# adds arbitrary text to a pre-existing image 'base'
	if [ $# -eq 0 ]
	then 
		echo "Please give enough arguments to the 'add_text' function.";echo "";exit;
	fi

	local base=$1		# the canvas over which we add the text
	local text=$2		# the text itself
	local fontname=$3
	local fontsizepc=$4	# font size, in percents of the base image height
	local fontcolor=$5
	local ypc=$6		# distance (of the top of the text) from the top of the frame, in percents of the base image height
	local align="$7"; if [ -z $align ]; then align="center"; fi

	local base_width=`identify -format "%w" "${base}"`
	local base_height=`identify -format "%h" "${base}"`
	local y=$( pc2pixel $ypc $base_height )				# ypc, now in pixels
	local fontsize=$( pc2pixel $fontsizepc $base_height )		# font size, now in pixels
	tempfile=temp${imgsuffix}

	local textwidth=$(( videoX - 2 * $( pc2pixel $margin $base_width ) ))	# maximum allowed width of the text, in pixels (text will be wrapped to the next line if longer)

	if [ ! -z "${text}" ]	# we only add the text overlay if the 'text' string is not empty
	then
		$convert -size "${textwidth}x" -background "${bg_color}" -font "${fontname}" -pointsize ${fontsize} -fill "${fontcolor}" -gravity $align caption:"${text}" text${imgsuffix};
		# the above command makes two files: text-0.jpg and text-1.jpg (I don't know why). I only need the second one:
		#mv text-1${imgsuffix} text${imgsuffix}; 
		composite -gravity North -geometry +0+${y} text${imgsuffix} ${base} "${tempfile}"; mv "${tempfile}" "${base}" ;
		#rm text-?${imgsuffix}
		rm text${imgsuffix}
	fi
}

function add_image {
	if [ $# -eq 0 ]
	then 
		echo "Please give enough arguments to the 'add_image' function.";echo "";exit;
	fi
	local base="$1"
	local image="$2"
	local resize="$3"
	local align="$4"; if [ -z $align ]; then align="center"; fi
	local xpc="$5"; if [ -z $xpc ]; then xpc="0"; fi
	local ypc="$6"; if [ -z $ypc ]; then ypc="0"; fi

	local base_width=`identify -format "%w" "${base}"`
	local x=$( pc2pixel $xpc $base_width )		# xpc, now in pixels
	local base_height=`identify -format "%h" "${base}"`
	local y=$( pc2pixel $ypc $base_height )		# ypc, now in pixels
	image_resized="image_resized${imgsuffix}"
	tempfile=temp${imgsuffix}

	if [ $resize == "fit" ]			# we fit the image to the slide, respecting the margins
	then
		local image_width=$(( videoX - 2 * $( pc2pixel $margin $base_width ) ))
		local image_height=$(( videoY - 2 * $( pc2pixel $margin $base_height ) ))
		$convert "$image" -resize "${image_width}x${image_height}" -unsharp 0x1 "${image_resized}"
	elif [ $resize == "fit-tight" ]		# like 'fit', but neglects margins
	then
		local image_width=$(( videoX ))
		local image_height=$(( videoY ))
		$convert "$image" -resize "${image_width}x${image_height}" -unsharp 0x1 "${image_resized}"
	elif [ $resize = "shrink" ] 
	then
		local image_width=$(( videoX - 2 * $( pc2pixel $margin $base_width ) ))
		local image_height=$(( videoY - 2 * $( pc2pixel $margin $base_height ) ))
		$convert "$image" -resize "${image_width}x${image_height}>" -unsharp 0x1 "${image_resized}"
	elif [ $resize == "shrink-tight" ]		# like 'shrink', but neglects margins
	then
		local image_width=$(( videoX ))
		local image_height=$(( videoY ))
		$convert "$image" -resize "${image_width}x${image_height}>" -unsharp 0x1 "${image_resized}"
	elif [ $resize = "orig" ] 		# keeps original image. May cause problems if image is larger than canvas
	then
		cp "$image" "${image_resized}"
	#elif [ ${resize:0:1} == "%" -o ${resize: -1} == "%" ]
	elif [ ${resize: -1} == "%" ]	# if the last character of resize is '%' (e.g. resize='50%'), we read it as percentage
	then
		imghpc=${resize:0:${#resize}-1}	# the rest of the string, the image height in percents
		# !!!!! if imghpc is a number:
		#re='^[0-9]+$'			# for positive integers
		re='^[0-9]+([.][0-9]+)?$'	# for positive decimal numbers
		if ! [[ $imghpc =~ $re ]] ; then
		   echo "Invalid image resize parameter '$resize' in '$seqfile' or '$templatefile'. Aborting."
		   echo ""
		   exit 1
		fi
		imgh=$( pc2pixel $imghpc $base_height )
		$convert "$image" -resize "x${imgh}" -unsharp 0x1 "${image_resized}"
		# !!! else print error message and exit
	else
		echo "Resize parameter '"${resize}"' not recognized by 'add_image' function. Options are fit, fit-tight, shrink, shrink-tight, orig, or a percent number like '50%'. Aborting.";echo; exit 1;
	fi

	composite -gravity $align -geometry +${x}+${y} "${image_resized}" "${base}" "${tempfile}"; mv "${tempfile}" "${base}" ;

	rm "${image_resized}"
}
function check_file {
	file=$1
	if [ -z "${file}" ]
	then
		echo "No input file is given for the segment in '"${seqfile}"'. Aborting."
		echo ""
		exit 1 
	elif [ ! -e "${file}" ] 
	then
		echo "Input file '"${file}"' given in '"${seqfile}"' does not exist. Aborting."
		echo ""
		exit 1
	fi
}
function video_start_frame {
	if [ $# -ne 3 ]
	then 
		echo "Please give enough arguments to the 'video_start_frame' function.";echo "";exit;
	fi
	local video="$1"
	local start_time="$2"
	local start_frame="$3"
	
	# first frame of the video at the time 'start_time'
	$ffmpeg -loglevel quiet -i "$video" -r 1 -ss ${start_time} -t 1 -f image2 "snapshot%03d${imgsuffix}"

	# resizing/padding the last frame using the add_image function:
	$convert -size ${videoX}x${videoY} xc:${bg_color} "${start_frame}"	# first, empty canvas
	add_image "${start_frame}" "snapshot001${imgsuffix}" fit-tight center 0 0

	rm snapshot???${imgsuffix}
}
function video_last_frame {
	if [ $# -eq 0 ]
	then 
		echo "Please give enough arguments to the 'video_last_frame' function.";echo "";exit;
	fi
	local video="$1"
	local last_frame="$2"
	
        # last frame of the video	
	nb_frames_line=`$ffprobe -loglevel quiet -show_streams "${video}"|grep nb_frames|head -1`
	nframes=`echo ${nb_frames_line##*=}`
	rm "${last_frame}"
	while [ ! -e "${last_frame}" ] && [ $nframes -gt 0 ]
	do
		nframes=$(( nframes - 1 ))
		$ffmpeg -y -loglevel quiet -i "${video}" -vf "select='eq(n,"$nframes")'" -vframes 1 "${last_frame}"
	done
}
function pc2pixel {	# takes a value in percent and converts it to pixels
	local percent=$1
	local total=$2
	#echo $(( percent*total / 100 ))     # converted from percent to pixels (old, no support for decimal input numbers)
	echo $(echo "scale=0;$percent*$total/100" | bc ) # converted from percent to pixels
}
function make_static_video {
	if [ $# -eq 0 ]
	then 
		echo "Please give enough arguments to the 'make_static_video' function.";echo "";exit;
	fi
	local static_image="$1"
	local static_time="$2"
	
	# METHOD 1: duplicating the still image many times and make a video
		NREPEAT=$(( FPS * $static_time/1000 ))
		if [ $NREPEAT -gt 0 ]
		then
			for (( j=1; j <= $NREPEAT; j++ )); do jjj=`printf '%03d' $j`; ln -s $static_image frame${jjj}${imgsuffix};done
			# METHOD 1.1: using mencoder (higher quality, smaller file size)
			$mencoder -really-quiet mf://frame*${imgsuffix} -ovc copy -mf fps=${FPS} -vf scale=${videoX}:${videoY} -o slide${iii}_0.avi
#			# METHOD 1.2: using ffmpeg (lower quality, larger file size)
#			$ffmpeg -f image2 -i frame%03d${imgsuffix} -r ${FPS} -s ${videoX}x${videoY} -c:v mjpeg slide${iii}_0.avi >/dev/null 2>&1
			rm frame???${imgsuffix}
		fi

	## METHOD 2: making a static video drectly using ffmpeg 
	## Video quality is lower than method 1.1. Slightly takes longer. Timing (division by 1000) gives only integer second times that must be fixed.
	#$ffmpeg -y -loglevel quiet -loop 1 -i $static_image -an -c:v mjpeg -t $((static_time / 1000)) -r $FPS slide${iii}_0.avi
}
function make_morphed_video {	# this function makes a video of fading one image to the other
	first=$1	# first image, beginning of morph
	second=$2	# second image, end of morph
	time=$3		# total duration of the morphing process, in miliseconds
	morph_output=$4
	NMORPH=$(( FPS * ${time} / 1000 ))	# number of morphing frames. Update this calculation with bc !!!
	if [ $NMORPH -gt 0 ]
	then
		if [ -e "${second}" ]
		then
			$convert "${first}" "${second}" -delay 5 -morph ${NMORPH} frame%03d${imgsuffix}

			# using mencoder (higher quality)
			$mencoder -really-quiet mf://frame*${imgsuffix} -ovc copy -mf fps=${FPS} -vf scale=${videoX}:${videoY} -o "${morph_output}"
#			# using ffmpeg (lower quality)
#			$ffmpeg -f image2 -i frame%03d${imgsuffix} -r ${FPS} -s ${videoX}x${videoY} -c:v mjpeg "${morph_output}" >/dev/null 2>&1
			rm frame???${imgsuffix}
		fi
	fi
}
function get_file {
	local file="$1"
	if [ -z "${file}" ]
	then
		echo "No input file is given for the segment in '"${seqfile}"'. Aborting."
		echo ""
		exit 1
	elif is_absolute_path "${file}"	# if the file is given as an absolute path
	then
		if [ ! -e "${file}" ]
		then
			echo "Input file '"${file}"' given in '"${seqfile}"' does not exist. Aborting."
			echo ""
			exit 1;
		else
			echo "${file}"	# we return the same absolute path
		fi
	# we check if the file exists (1) next to sequence file, or (2) in the working directory, or (3) in seq2vid's example directory. If succeed, we set the absolute path and exit. If not, we print error (and exit).
	elif [ -e "${SequencePath}${PathSep}${file}" ]
	then
		echo "${SequencePath}${PathSep}${file}"
	elif [ -e "${WorkingPath}${PathSep}${file}" ]
	then
		echo "${WorkingPath}${PathSep}${file}"
	elif [ -e "${ExamplesPath}${PathSep}${file}" ]
	then
		echo "${ExamplesPath}${PathSep}${file}"
	else
		echo "Input file '"${file}"' given in '"${seqfile}"' does not exist. "
		echo "looked for it next to the sequence file, in the working directory,"
		echo "and even in seq2vid's examples directory, with no luck. Aborting."
		echo ""
		exit 1;
	fi
}
function video_width {
	if [ $# -ne 1 ]
	then 
		echo "Please give video name to the 'video_dimensions' function.";echo "" #;exit;
	fi
	local video="$1"
	
	width_line=`$ffprobe -loglevel quiet -show_streams "${video}"|grep "width="|head -1`
	video_dimensions_width=${width_line##*=}

	echo $video_dimensions_width
}
function video_height {
	if [ $# -ne 1 ]
	then 
		echo "Please give video name to the 'video_dimensions' function.";echo "" #;exit;
	fi
	local video="$1"
	
	height_line=`$ffprobe -loglevel quiet -show_streams "${video}"|grep "height="|head -1`
	video_dimensions_height=${height_line##*=}

	echo $video_dimensions_height
}
